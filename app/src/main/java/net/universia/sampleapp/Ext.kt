package net.universia.sampleapp

import android.content.Context
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

inline fun <I, reified O> I.convert(): O = Gson().run {
    val gson = Gson()
    val json = Gson().toJson(this)
    return gson.fromJson(json, object : TypeToken<O>() {}.type)
}

inline fun <reified T> Map<String, Any>.toDataClass(): T {
    return convert()
}

fun View.isVisible(
    boolean: Boolean
) = apply {
    visibility = if (boolean) View.VISIBLE else View.GONE
}

fun Context.showToast(
    message: String
) = Toast.makeText(this, message, Toast.LENGTH_SHORT).show()

fun loge(message: String) = Log.e("GLAGOS", message)

fun FragmentManager.cleanFragments(){
    if (findFragmentById(R.id.main_container) != null) {
        popBackStack()
    }
}

fun AppCompatActivity.navigate(fragment: Fragment) {
    if(!fragment.isAdded) {
        this.supportFragmentManager
            .beginTransaction()
            .add(R.id.main_container, fragment)
            .addToBackStack(null)
            .commit()
    }
}

