package net.universia.sampleapp.model

data class ResultCheckIdps(
        val result: Boolean,
        val apiKey: String?
    )