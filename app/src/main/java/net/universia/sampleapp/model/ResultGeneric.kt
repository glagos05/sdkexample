package net.universia.sampleapp.model

data class ResultGeneric<out T, out R>(
    val data: T? = null,
    val error: R? = null
)

fun <T, R>ResultGeneric<T, R>.isValidData(
    onSuccess: (T) -> Unit,
    onFailure: (R) -> Unit
){
    if (data != null && error == null) onSuccess(data)
    else onFailure(error!!)
}
