package net.universia.sampleapp.source

import android.content.Context
import androidx.fragment.app.Fragment
import com.universia.digitalcredential.api.data.error.LoginError
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job

interface ApiHelper {

    fun initSdkComponents(
        context: Context
    )

    fun logoOut(
        scope: CoroutineScope,
        onFailure: (String) -> Unit,
        onSuccess: () -> Unit
    ): Job

    fun checkIdp(
        scope: CoroutineScope,
        context: Context,
        entityId: String,
        onFailure: (String) -> Unit,
        onSelectIdp: (Fragment) -> Unit,
        onInitSdk: (Boolean) -> Unit
    ): Job

    fun hasInitializedSdk(): Boolean

    fun login(
        scope: CoroutineScope,
        onFailure: (LoginError) -> Unit,
        onSuccess: (Boolean) -> Unit
    ): Job

    fun getConfig(
        scope: CoroutineScope,
        onFailure: (String) -> Unit,
        onSuccess: (MutableList<HashMap<String, Any>>) -> Unit
    ): Job

    fun getEntityConfig(
        scope: CoroutineScope,
        context: Context,
        entityId: String,
        onFailure: (String) -> Unit,
        onSuccess: (String?) -> Unit
    ): Job

    fun getEntityToken(
        scope: CoroutineScope,
        onFailure: (String) -> Unit,
        onSuccess: (Boolean) -> Unit
    ): Job

    fun showCredential(
        scope: CoroutineScope,
        context: Context,
        entityId: String,
        onFailure: (String) -> Unit,
        onSuccess: (Fragment) -> Unit
    ): Job

    fun updateCredential(
        scope: CoroutineScope,
        onFailure: (String) -> Unit,
        onSuccess: (Map<String, Any>) -> Unit
    ): Job

    fun updateCredentialStatus(
        scope: CoroutineScope,
        onFailure: (String) -> Unit,
        onSuccess: (Boolean) -> Unit
    ): Job

    fun createKey(
        scope: CoroutineScope,
        onFailure: (String) -> Unit,
        onSuccess: (Boolean) -> Unit
    ):Job
    
    fun emitKey(
        scope: CoroutineScope,
        onFailure: (String) -> Unit,
        onSuccess: (Boolean) -> Unit
    ): Job

    fun createCustomerRequest(
        scope: CoroutineScope,
        onFailure: (String) -> Unit,
        onSuccess: (Boolean) -> Unit
    ): Job

    fun cancelDataAssignment(
        scope: CoroutineScope,
        onFailure: (String) -> Unit,
        onSuccess: (Boolean) -> Unit
    ): Job

}