package net.universia.sampleapp.source

import androidx.fragment.app.Fragment

interface TemplateHelper {
    fun createUser(): Fragment
    fun digitalCredential(): Fragment
    fun userSettings(): Fragment
    fun userTerms(): Fragment
}