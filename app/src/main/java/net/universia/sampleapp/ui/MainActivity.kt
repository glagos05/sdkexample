package net.universia.sampleapp.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.universia.digitalcredential.api.data.error.LoginError
import com.universia.digitalcredential.api.listeners.DigitalCredentialLoginListener
import com.universia.templatesdk.listeners.LogoutListener
import com.universia.templatesdk.listeners.TemplateIdpListener
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.plus
import net.universia.sampleapp.*

@InternalCoroutinesApi
@ExperimentalCoroutinesApi
class MainActivity : AppCompatActivity(),
    LogoutListener {

    companion object {
        //UNIVERSIA AUTH
        private const val ENTITY_ID = "c2d41955-a0b4-43ed-b5c1-f5464251f723"
        //CUIB, GOOGLE AUTH
        /*private const val ENTITY_ID = "96fd930d-0bfb-4180-ae40-8e1e77c02e3f"*/
    }

    private val vm by lazy {
        ViewModelProvider(this)
            .get(MainVm::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bt_initSdk.setOnClickListener {
            SdkHelper.checkIdp(
                scope = vm.viewModelScope + Dispatchers.IO,
                context = applicationContext,
                entityId = ENTITY_ID,
                onFailure = ::showToast,
                onSelectIdp = ::navigate,
                onInitSdk = {
                    updateButtonsInit(isInitialized = it)
                }
            )
        }

        bt_doLogin.setOnClickListener {
            SdkHelper.login(
                scope = vm.viewModelScope + Dispatchers.IO,
                onFailure = { error ->
                    loge("Error: $error")
                    when (error) {
                        is LoginError.ConfigError,
                        is LoginError.TokenError,
                        is LoginError.OauthError,
                        is LoginError.GenericError,
                        is LoginError.UnknownError -> SdkHelper.logoOut(
                            scope = vm.viewModelScope + Dispatchers.IO,
                            onFailure = ::showToast,
                            onSuccess = {
                                updateButtons(false)
                            }
                        )

                        /**
                         * LoginError.PassCodeRequiredError - ¡New feature!
                         */
                        is LoginError.PassCodeRequiredError -> showToast(error.toString())
                    }
                },
                onSuccess = {
                    updateButtons(isLogged = it)
                }
            )
        }

        bt_showCredential.setOnClickListener {
            SdkHelper.showCredential(
                scope = vm.viewModelScope + Dispatchers.IO,
                context = applicationContext,
                entityId = ENTITY_ID,
                onFailure = ::showToast,
                onSuccess = ::navigate
            )
        }

        bt_getConfig.setOnClickListener {
            SdkHelper.getConfig(
                scope = vm.viewModelScope + Dispatchers.IO,
                onFailure = ::showToast
            ) {
                showToast("GET CONFIG: $it")
            }
        }

        bt_getEntityConfig.setOnClickListener {
            SdkHelper.getEntityConfig(
                scope = vm.viewModelScope + Dispatchers.IO,
                context = applicationContext,
                entityId = ENTITY_ID,
                onFailure = ::showToast,
                onSuccess = {
                    showToast("GET ENTITY CONFIG: $it")
                }
            )
        }

        bt_getEntityToken.setOnClickListener {
            SdkHelper.getEntityToken(
                scope = vm.viewModelScope + Dispatchers.IO,
                onFailure = ::showToast,
                onSuccess = {
                    showToast("GET ENTITY CONFIG: $it")
                }
            )
        }

        bt_showUserConfig.setOnClickListener {
            navigate(fragment = SdkHelper.userSettings())
        }

        bt_showUserTerms.setOnClickListener {
            navigate(fragment = SdkHelper.userTerms())
        }

        bt_doLogout.setOnClickListener {
            SdkHelper.logoOut(
                scope = vm.viewModelScope + Dispatchers.IO,
                onFailure = ::showToast,
                onSuccess = {
                    updateButtons(false)
                }
            )
        }
    }

    private fun updateButtonsInit(
        isInitialized: Boolean
    ){
        if (isInitialized) {
            layoutInit.isVisible(false)
            layoutLogin.isVisible(true)
            layoutFeatures.isVisible(false)
        } else {
            layoutInit.isVisible(true)
            layoutLogin.isVisible(false)
            layoutFeatures.isVisible(false)
        }
    }

    private fun updateButtons(
        isLogged: Boolean
    ) {
        if (isLogged) {
            layoutInit.isVisible(false)
            layoutLogin.isVisible(false)
            layoutFeatures.isVisible(true)
        } else {
            layoutInit.isVisible(true)
            layoutLogin.isVisible(false)
            layoutFeatures.isVisible(false)
        }
    }



    /**
     * onLogout - ¡New feature!
     *
     * @param success
     */
    override fun onLogout(success: Boolean) {
        loge(message = "ONLOGOUT SUCCESS: $success")
        if (success) {
            updateButtons(false)
            supportFragmentManager.cleanFragments()
        } else showToast("User settings logout error")
    }
}