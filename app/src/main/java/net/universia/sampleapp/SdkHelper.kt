package net.universia.sampleapp

import android.content.Context
import androidx.fragment.app.Fragment
import com.universia.digitalcredential.api.DigitalCredentialApi
import com.universia.digitalcredential.api.data.CreateCustomerRequestBody
import com.universia.digitalcredential.api.data.KeysProvider
import com.universia.digitalcredential.api.data.UpdateCredentialBody
import com.universia.digitalcredential.api.data.error.*
import com.universia.digitalcredential.api.listeners.*
import com.universia.templatesdk.DigitalCredentialTemplate
import com.universia.templatesdk.listeners.CheckIdpsListener
import com.universia.templatesdk.listeners.TemplateIdpListener
import com.universia.templatesdk.models.UserInfo
import kotlinx.coroutines.*
import net.universia.sampleapp.model.ResultCheckIdps
import net.universia.sampleapp.model.ResultGeneric
import net.universia.sampleapp.model.isValidData
import net.universia.sampleapp.source.ApiHelper
import net.universia.sampleapp.source.TemplateHelper
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

/**
 * SdkHelper - Singleton
 *
 * Object instead of Class,
 * because on this example don't need a constructor
 *
 * Contains all enabled methods of
 * DigitalCredentialApi and DigitalCredentialTemplate
 */
object SdkHelper : TemplateHelper, ApiHelper {

    lateinit var api: DigitalCredentialApi
    lateinit var template: DigitalCredentialTemplate
    lateinit var apiKey: String

    /**
     * getTemplateVersion
     *
     * @return String
     */
    fun getTemplateVersion() = DigitalCredentialTemplate.getVersion()

    /**
     * selectIdp
     *
     * @return Fragment
     */
    private fun selectIdp(
        onSuccess: (String) -> Unit
    ) = DigitalCredentialTemplate.selectIdp(
        listener = object : TemplateIdpListener{
            override fun onIdpSelected(apiKey: String) {
                loge("SELECT IDP: $apiKey")
                onSuccess(apiKey)
            }
        }
    )

    /**
     * createUser
     *
     * @return Fragment
     */
    override fun createUser() = template.createUser()

    /**
     * digitalCredential
     *
     * @return Fragment
     */
    override fun digitalCredential() = template.digitalCredential()

    /**
     * userSettings
     *
     * @return Fragment
     */
    override fun userSettings() = template.userSettings()

    /**
     * userTerms
     *
     * @return Fragment
     */
    override fun userTerms() = template.userTerms()

    /**
     * initSdkComponents
     *
     * @param context
     */
    override fun initSdkComponents(
        context: Context
    ) {
        api = DigitalCredentialApi
            .create(
                apiKey = apiKey,
                context = context
            )

        template = DigitalCredentialTemplate
            .create(
                apiKey = apiKey,
                context = context
            )
    }

    override fun hasInitializedSdk() = ::api.isInitialized && ::template.isInitialized

    /**
     * logoOut
     *
     * @param onSuccess
     */
    override fun logoOut(
        scope: CoroutineScope,
        onFailure: (String) -> Unit,
        onSuccess: () -> Unit
    ) = scope.launch {

        val response = suspendCoroutine<ResultGeneric<Boolean, String>> { continuation ->
            api.doLogout(listener = object : DoLogoutListener {
                override fun onLogoutFailure(error: LogoutError) {
                    loge("LOGOUT ERROR: $error")
                    continuation.resume(ResultGeneric(error = error.toString()))
                }

                override fun onLogoutSuccess() {
                    loge("LOGOUT SUCCESS: true")
                    continuation.resume(ResultGeneric(data = true))
                }
            })
        }

        withContext(Dispatchers.Main){
            response.isValidData(
                onFailure = onFailure,
                onSuccess = {
                    onSuccess()
                }
            )
        }
    }

    /**
     * checkIdp
     *
     * @param scope
     * @param context
     * @param entityId
     * @param onFailure
     * @param onSelectIdp
     * @return Job
     */
    override fun checkIdp(
        scope: CoroutineScope,
        context: Context,
        entityId: String,
        onFailure: (String) -> Unit,
        onSelectIdp: (Fragment) -> Unit,
        onInitSdk: (Boolean) -> Unit
    ) = scope.launch {

        val response = suspendCoroutine<ResultGeneric<ResultCheckIdps, String>> { continuation ->

            DigitalCredentialTemplate.checkIdp(
                entityId = entityId,
                context = context,
                listener = object : CheckIdpsListener {
                    override fun onGetEntityConfigFailure(error: GetEntityConfigError) {
                        continuation.resume(
                            ResultGeneric(error = error.toString())
                        )
                    }

                    override fun shouldShowIdpScreen(
                        result: Boolean,
                        apiKey: String?
                    ) {
                        continuation.resume(
                            ResultGeneric(
                                data = ResultCheckIdps(
                                    result = result,
                                    apiKey = apiKey
                                )
                            )
                        )
                    }
                }
            )
        }

        loge("CHECK IDP: $response")

        withContext(Dispatchers.Main){
            response.isValidData(
                onSuccess = {
                    if (it.result) {

                        val fr = selectIdp(
                            onSuccess = { key ->
                                apiKey = key
                                initSdkComponents(context = context)
                                onInitSdk(hasInitializedSdk())
                            }
                        )

                        onSelectIdp(fr)

                    }
                    else if (!it.apiKey.isNullOrEmpty() && !it.result) {

                        apiKey = it.apiKey
                        initSdkComponents(context = context)
                        onInitSdk(hasInitializedSdk())

                    } else onFailure("Error desconocido")
                },
                onFailure = onFailure
            )
        }
    }

    /**
     * login
     *
     * @param scope
     * @param onFailure
     * @param onSuccess
     * @return
     */
    override fun login(
        scope: CoroutineScope,
        onFailure: (LoginError) -> Unit,
        onSuccess: (Boolean) -> Unit
    ) = scope.launch {

        val response = suspendCoroutine<ResultGeneric<Boolean, LoginError>> { continuation ->
            template.doLogin(
                listener = object : DigitalCredentialLoginListener{
                    override fun onFailure(error: LoginError) {
                        loge("DO LOGIN ERROR: $error")
                        continuation.resume(ResultGeneric(error = error))
                    }

                    override fun onSuccess(response: Map<String, Any>) {
                        loge("DO LOGIN: SUCCESS")
                        continuation.resume(ResultGeneric(data = true))
                    }

                }
            )
        }

        withContext(Dispatchers.Main) {
            response.isValidData(
                onSuccess = onSuccess,
                onFailure = onFailure
            )
        }
    }

    /**
     * getConfig
     *
     * @param scope
     * @param onSuccess
     * @return Job
     */
    override fun getConfig(
        scope: CoroutineScope,
        onFailure: (String) -> Unit,
        onSuccess: (MutableList<HashMap<String, Any>>) -> Unit
    ) = scope.launch {
        val response = suspendCoroutine<ResultGeneric<MutableList<HashMap<String, Any>>, String>> { continuation ->
            api.getCustomerRequestTypes(listener = object : GetCustomerRequestTypeListener {
                override fun onGetCustomerRequestTypeFailure(error: GetCustomerRequestTypesError) {
                    loge("TYPE REQUEST ERROR: $error")
                    continuation.resume(ResultGeneric(error = error.toString()))
                }

                override fun onGetCustomerRequestTypeSuccess(customerRequestType: MutableList<HashMap<String, Any>>) {
                    loge("TYPE REQUEST SUCCESS: $customerRequestType")
                    continuation.resume(ResultGeneric(data = customerRequestType))
                }
            })
        }

        withContext(Dispatchers.Main) {
            response.isValidData(
                onSuccess = onSuccess,
                onFailure = onFailure
            )
        }
    }

    /**
     * getEntityConfig replace of getUniversityConfig
     *
     * @param scope
     * @param context
     * @param entityId
     * @param onSuccess
     * @return Job
     */
    override fun getEntityConfig(
        scope: CoroutineScope,
        context: Context,
        entityId: String,
        onFailure: (String) -> Unit,
        onSuccess: (String?) -> Unit
    ) = scope.launch {
        val response = suspendCoroutine<ResultGeneric<String?, String>> { continuation ->
            DigitalCredentialApi.getEntityConfig(
                context = context,
                entityId = entityId,
                listener = object : GetEntityConfigListener {
                    override fun onGetEntityConfigFailure(error: GetEntityConfigError) {
                        loge("GET ENTITY CONFIG ERROR: $error")
                        continuation.resume(ResultGeneric(error = error.toString()))
                    }

                    override fun onGetEntityConfigSuccess(response: Map<String, Any>) {
                        val apiKey = try {
                            (response["data"] as? Map<String, String>)
                                    ?.let { data ->
                                        (data["idps"] as? ArrayList<Map<String, String>>)
                                            ?.firstOrNull()
                                            ?.get("apikey")
                                    }

                        } catch (e: Exception){
                            loge("GET ENTITY CONFIG ERROR(apikey): $e")
                            null
                        }

                        loge("GET ENTITY CONFIG: $apiKey")

                        continuation.resume(ResultGeneric(data = apiKey))
                    }
                }
            )
        }

        withContext(Dispatchers.Main) {
            response.isValidData(
                onSuccess = onSuccess,
                onFailure = onFailure
            )
        }
    }

    /**
     * getEntityToken replace of getUniversityToken
     *
     * @param scope
     * @param onSuccess
     * @return Job
     */
    override fun getEntityToken(
        scope: CoroutineScope,
        onFailure: (String) -> Unit,
        onSuccess: (Boolean) -> Unit
    ) = scope.launch {
        val response = suspendCoroutine<ResultGeneric<Boolean, String>> { continuation ->
            api.getEntityToken(
                listener = object : GetEntityTokenListener {
                    override fun onFailure(error: EntityTokenError) {
                        loge("GET ETT TOKEN ERROR: $error")
                        continuation.resume(ResultGeneric(error = error.toString()))
                    }

                    override fun onSuccess(entityToken: Map<String, Any>) {
                        loge("GET ETT TOKEN: $entityToken")
                        continuation.resume(ResultGeneric(data = true))
                    }
                }
            )
        }

        withContext(Dispatchers.Main) {
            response.isValidData(
                onSuccess = onSuccess,
                onFailure = onFailure
            )
        }
    }

    /**
     * showCredential
     *
     * @param scope
     * @param context
     * @param entityId
     * @param onFailure
     * @param onSuccess
     * @return Job
     */
    override fun showCredential(
        scope: CoroutineScope,
        context: Context,
        entityId: String,
        onFailure: (String) -> Unit,
        onSuccess: (Fragment) -> Unit
    ) = scope.launch {

        val response = suspendCoroutine<ResultGeneric<UserInfo, String>> { continuation ->
            api.getCredential(
                listener = object : GetCredentialListener {
                    override fun onFailure(error: UserError) {
                        when (error) {
                            is UserError.LoginRequired,
                            is UserError.AccessDenied -> {
                                continuation.resume(ResultGeneric(error = "No se que hacer en este punto"))
                            }

                            is UserError.Unknown,
                            is UserError.Forbidden,
                            is UserError.NotFound,
                            is UserError.InternalError,
                            is UserError.BadGateway,
                            is UserError.InvalidRequest,
                            is UserError.Timeout,
                            is UserError.InvalidJsonFormat -> continuation.resume(ResultGeneric(error = error.toString()))
                        }
                    }

                    override fun onSuccess(userInfo: Map<String, Any>) {
                        continuation.resume(ResultGeneric(data = userInfo.toDataClass()))
                    }
                }
            )
        }

        withContext(Dispatchers.Main) {
            response.isValidData(
                onSuccess = {
                    when (it.status) {
                        0 -> onSuccess(template.createUser())
                        1 -> onSuccess(template.digitalCredential())
                    }
                },
                onFailure = onFailure
            )
        }
    }

    /**
     * updateCredential
     *
     * @param scope
     * @param onSuccess
     * @return Job
     */
    override fun updateCredential(
        scope: CoroutineScope,
        onFailure: (String) -> Unit,
        onSuccess: (Map<String, Any>) -> Unit
    ) = scope.launch {
        val response = suspendCoroutine<ResultGeneric<Map<String, Any>, String>> { continuation ->
            api.updateCredential(
                request = UpdateCredentialBody(
                    interest = listOf(),
                    nationality = "",
                    termsOfService = listOf(),
                    personalMail = ""
                ),
                listener = object : UpdateCredentialListener {
                    override fun onUpdateCredentialFailure(error: UpdateCredentialError) {
                        loge("UPDATE CREDENTIAL ERROR: $error")
                        continuation.resume(ResultGeneric(error = error.toString()))
                    }

                    override fun onUpdateCredentialSuccess(response: Map<String, Any>) {
                        continuation.resume(ResultGeneric(data = response))
                    }

                }
            )
        }

        withContext(Dispatchers.Main) {
            response.isValidData(
                onSuccess = onSuccess,
                onFailure = onFailure
            )
        }
    }

    /**
     * updateCredentialStatus
     *
     * @param scope
     * @param onSuccess
     * @return Job
     */
    override fun updateCredentialStatus(
        scope: CoroutineScope,
        onFailure: (String) -> Unit,
        onSuccess: (Boolean) -> Unit
    ) = scope.launch {
        val response = suspendCoroutine<ResultGeneric<Boolean, String>> { continuation ->
            api.updateCredentialStatus(
                listener = object : UpdateCredentialStatusListener {
                    override fun onUpdateCredentialStatusFailure(error: UpdateCredentialStatusError) {
                        loge("UPDATE CREDENTIAL STATUS ERROR: $error")
                        continuation.resume(ResultGeneric(error = error.toString()))
                    }

                    override fun onUpdateCredentialStatusSuccess() {
                        continuation.resume(ResultGeneric(data = true))
                    }
                }
            )
        }

        withContext(Dispatchers.Main) {
            response.isValidData(
                onSuccess = onSuccess,
                onFailure = onFailure
            )
        }
    }

    /**
     * createKey
     *
     * @param scope
     * @param onSuccess
     * @return Job
     */
    override fun createKey(
        scope: CoroutineScope,
        onFailure: (String) -> Unit,
        onSuccess: (Boolean) -> Unit
    ) = scope.launch {
        val response = suspendCoroutine<ResultGeneric<Boolean, String>> { continuation ->
            api.createKey(
                provider = KeysProvider.Salto,
                forceKey = null,
                listener = object : CreateKeyListener {
                    override fun onCreateKeysFailure(error: CreateKeyError) {
                        loge("CREATE KEY ERROR: $error")
                        continuation.resume(ResultGeneric(error = error.toString()))
                    }

                    override fun onCreateKeysSuccess() {
                        continuation.resume(ResultGeneric(data = true))
                    }

                }
            )
        }

        withContext(Dispatchers.Main) {
            response.isValidData(
                onSuccess = onSuccess,
                onFailure = onFailure
            )
        }
    }

    /**
     * emitKey
     *
     * @param scope
     * @param onSuccess
     * @return Job
     */
    override fun emitKey(
        scope: CoroutineScope,
        onFailure: (String) -> Unit,
        onSuccess: (Boolean) -> Unit
    ) = scope.launch {
        val response = suspendCoroutine<ResultGeneric<Boolean, String>> { continuation ->
            api.emitKey(
                provider = KeysProvider.Salto,
                listener = object : EmitKeyListener {
                    override fun onEmitFailure(error: EmitKeyError) {
                        loge("EMIT KEY ERROR: $error")
                        continuation.resume(ResultGeneric(error = error.toString()))
                    }

                    override fun onEmitKeySuccess() {
                        continuation.resume(ResultGeneric(data = true))
                    }
                }
            )
        }

        withContext(Dispatchers.Main) {
            response.isValidData(
                onSuccess = onSuccess,
                onFailure = onFailure
            )
        }
    }

    /**
     * createCustomerRequest
     *
     * @param scope
     * @param onSuccess
     * @return Job
     */
    override fun createCustomerRequest(
        scope: CoroutineScope,
        onFailure: (String) -> Unit,
        onSuccess: (Boolean) -> Unit
    ) = scope.launch {
        val response = suspendCoroutine<ResultGeneric<Boolean, String>> { continuation ->
            api.createCustomerRequest(
                requestBody = CreateCustomerRequestBody(
                    email = "",
                    message = "",
                    subject = "",
                    requestTypeId = "",
                    app = "",
                    tokenRecaptcha = "",
                    captchaType = ""
                ),
                listener = object : CreateCustomerRequestListener {
                    override fun onCreateCustomerRequestFailure(error: CreateCustomerRequestError) {
                        loge("CREATE REQUEST ERROR: $error")
                        continuation.resume(ResultGeneric(error = error.toString()))
                    }

                    override fun onCreateCustomerRequestSuccess() {
                        continuation.resume(ResultGeneric(data = true))
                    }
                }
            )
        }

        withContext(Dispatchers.Main) {
            response.isValidData(
                onSuccess = onSuccess,
                onFailure = onFailure
            )
        }
    }

    /**
     * cancelDataAssignment
     *
     * @param scope
     * @param onSuccess
     * @return Job
     */
    override fun cancelDataAssignment(
        scope: CoroutineScope,
        onFailure: (String) -> Unit,
        onSuccess: (Boolean) -> Unit
    ) = scope.launch {
        val response = suspendCoroutine<ResultGeneric<Boolean, String>> { continuation ->
            api.cancelDataAssignment(
                reasonOfLeaving = "",
                listener = object : CancelDataAssignmentListener {
                    override fun onCancelDataAssignmentFailure(error: CancelDataAssignmentError) {
                        loge("CANCEL DATA ERROR: $error")
                        continuation.resume(ResultGeneric(error = error.toString()))
                    }

                    override fun onCancelDataAssignmentSuccess() {
                        continuation.resume(ResultGeneric(data = true))
                    }
                }
            )
        }

        withContext(Dispatchers.Main) {
            response.isValidData(
                onSuccess = onSuccess,
                onFailure = onFailure
            )
        }
    }






}